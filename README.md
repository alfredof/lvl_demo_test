# README

##Welcome To LVL Food and Cocktail Repository!!!

This repository aims to store a set of recipes for meals and cocktails from Peru and that can be used according to your own tastes and interests

###How to use this repository?

The repository has 2 folders, one for storing food recipes and another for cocktail recipes, go to the corresponding folder and open the file that contains the recipe you want to see, each file contains a recipe and is named after the dish or cocktail whose recipe contains

###Where i can found the ingredients
If you are in Peru, just go to any market or super market (local markets usually have fresh and cheaper ingredients) 
If you are not in Peru, look fot Latin markets or Peruvian Markets and ask for ingredients

###Can I Add Recipes
Of course! just clone this repository and add the recipes you like, then execute the commit and push commands to be reflected in the repository

